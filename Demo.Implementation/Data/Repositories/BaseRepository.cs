﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Demo.Implementation.Core.Entities;

namespace Demo.Implementation.Data.Repositories
{
    public class BaseRepository<TEntity> where TEntity : class, IEntity
    {
        private readonly TodoItemContext _dbContext;

        public BaseRepository(TodoItemContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TEntity> GetAsync(int id)
        {
            return await _dbContext.Set<TEntity>().FindAsync(id);
        }

        public async Task<IEnumerable<TEntity>> GetAsync()
        {
            return await _dbContext.Set<TEntity>().ToListAsync();
        }

        public async Task InsertAsync(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _dbContext.Set<TEntity>().FindAsync(id);
            _dbContext.Set<TEntity>().Remove(entity ?? throw new InvalidOperationException());
            await _dbContext.SaveChangesAsync();
        }
    }
}