﻿using Demo.Implementation.Core.Entities;
using Demo.Implementation.Core.Repositories;

namespace Demo.Implementation.Data.Repositories
{
    public class TodoItemRepository : BaseRepository<TodoItem>, ITodoItemRepository
    {
        public TodoItemRepository(TodoItemContext dbContext) : base(dbContext)
        {
        }
    }
}
