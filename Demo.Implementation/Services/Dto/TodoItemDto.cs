﻿namespace Demo.Implementation.Services.Dto
{
    public class TodoItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AdditionalData { get; set; }
        public bool IsComplete { get; set; }
    }
}
