﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.Implementation.Services.Dto;

namespace Demo.Implementation.Services.Abstracts
{
    public interface ITodoItemService
    {
        Task<TodoItemDto> GetAsync(int id);
        Task<List<TodoItemDto>> GetAsync();
        Task InsertAsync(TodoItemDto todoItem);
        Task UpdateAsync(TodoItemDto todoItem);
        Task DeleteAsync(int id);
    }
}