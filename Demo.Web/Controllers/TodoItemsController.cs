﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Demo.Implementation.Services.Abstracts;
using Demo.Implementation.Services.Dto;
using Demo.Web.Models;

namespace Demo.Web.Controllers
{
    public class TodoItemsController : Controller
    {
        private readonly ITodoItemService _todoItemService;
        private readonly IMapper _mapper;

        public TodoItemsController(ITodoItemService todoItemService,
            IMapper mapper)
        {
            _todoItemService = todoItemService;
            _mapper = mapper;
        }

        // GET: TodoItems
        public async Task<ActionResult> Index()
        {
            var todoItems = await _todoItemService.GetAsync();

            return View(_mapper.Map<List<TodoItemModel>>(todoItems));
        }

        // GET: TodoItems/Details/5
        public async Task<ActionResult> Details(int id = 0)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var todoItem = await _todoItemService.GetAsync(id);
            if (todoItem == null)
            {
                return HttpNotFound();
            }
            return View(_mapper.Map<TodoItemModel>(todoItem));
        }

        // GET: TodoItems/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TodoItems/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TodoItemModel todoItem)
        {
            if (ModelState.IsValid)
            {
                await _todoItemService.InsertAsync(_mapper.Map<TodoItemDto>(todoItem));
                return RedirectToAction("Index");
            }

            var isMobileDevice = HttpContext.Request.Browser.IsMobileDevice;
            var userId = HttpContext.Request.Cookies["user-id"]?.Value;

            if (string.IsNullOrEmpty(userId))
            {
                userId = Guid.NewGuid().ToString();
                HttpContext.Response.Cookies.Add(new HttpCookie("user-id", userId)
                {
                    Expires = DateTime.Now.AddMinutes(10)
                });
            }

            todoItem.AdditionalData = $"MobileDevice: {isMobileDevice}, UserId: {userId}";

            return View(todoItem);
        }

        // GET: TodoItems/Edit/5
        public async Task<ActionResult> Edit(int id = 0)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var todoItem = await _todoItemService.GetAsync(id);
            if (todoItem == null)
            {
                return HttpNotFound();
            }
            return View(_mapper.Map<TodoItemModel>(todoItem));
        }

        // POST: TodoItems/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(TodoItemModel todoItem)
        {
            if (ModelState.IsValid)
            {
                await _todoItemService.UpdateAsync(_mapper.Map<TodoItemDto>(todoItem));
                return RedirectToAction("Index");
            }
            return View(todoItem);
        }

        // GET: TodoItems/Delete/5
        public async Task<ActionResult> Delete(int id = 0)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var todoItem = await _todoItemService.GetAsync(id);
            if (todoItem == null)
            {
                return HttpNotFound();
            }
            return View(_mapper.Map<TodoItemModel>(todoItem));
        }

        // POST: TodoItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await _todoItemService.DeleteAsync(id);
            return RedirectToAction("Index");
        }
    }
}
