using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using Demo.Implementation.Core.Entities;
using Demo.Implementation.Core.Repositories;
using Demo.Implementation.Data;
using Demo.Implementation.Data.Repositories;
using Demo.Implementation.Services.Abstracts;
using Demo.Implementation.Services.Concretes;
using Demo.Implementation.Services.Dto;
using Demo.Web.Models;

namespace Demo.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            InitializeContainer();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void InitializeContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<TodoItemService>().As<ITodoItemService>().InstancePerDependency();
            builder.RegisterType<TodoItemRepository>().As<ITodoItemRepository>().InstancePerLifetimeScope();
            builder.RegisterType<TodoItemContext>().InstancePerLifetimeScope();

            InitializeMapper(builder);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private void InitializeMapper(ContainerBuilder builder)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TodoItem, TodoItemDto>().ReverseMap();
                cfg.CreateMap<TodoItemDto, TodoItemModel>().ReverseMap();
            });
            
            var mapper = configuration.CreateMapper();
            builder.Register(ctx => mapper);
        }
    }
}
