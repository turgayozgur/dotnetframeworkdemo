# .NET Framework to .NET Core

The demo web application contains two projects. One of them is `Web` and the other one is `Implementation`. You don't need to change the architecture of the project. So, you can focus only on migrating to .NET Core.

What you need to do during this migration:

- Migrate the `Demo.Implementation` project to .NET Standard
- Migrate the `Demo.Web` project to .NET Core
- Change the third-party libraries with the compatible ones with .NET Core.
- Consider to seperate setting files by environment.
- Consider to make .NET Core related improvements like IHttpClientFactory, IHttpContextAccessor.
- Consider using built-in IoC Container.
- Developing the Dockerfile to run this demo project is plus as well.