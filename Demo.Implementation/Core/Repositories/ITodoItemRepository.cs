﻿using Demo.Implementation.Core.Entities;

namespace Demo.Implementation.Core.Repositories
{
    public interface ITodoItemRepository : IRepository<TodoItem> 
    {
    }
}
