﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.Implementation.Core.Entities;

namespace Demo.Implementation.Core.Repositories
{
    public interface IRepository<TEntity> where TEntity : class, IEntity
    {
        Task<TEntity> GetAsync(int id);
        Task<IEnumerable<TEntity>> GetAsync();
        Task InsertAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(int id);
    }
}