﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Demo.Implementation.Core.Entities;
using Demo.Implementation.Core.Repositories;
using Demo.Implementation.Services.Abstracts;
using Demo.Implementation.Services.Dto;

namespace Demo.Implementation.Services.Concretes
{
    public class TodoItemService : ITodoItemService
    {
        private readonly ITodoItemRepository _todoItemRepository;
        private readonly IMapper _mapper;

        public TodoItemService(ITodoItemRepository todoItemRepository,
            IMapper mapper)
        {
            _todoItemRepository = todoItemRepository;
            _mapper = mapper;
        }

        public async Task<TodoItemDto> GetAsync(int id)
        {
            var todoItem = await _todoItemRepository.GetAsync(id);

            using (var httpClient = new HttpClient())
            {
                await httpClient.GetAsync(ConfigurationManager.AppSettings["TodoSearchUrl"]);
            }

            return _mapper.Map<TodoItemDto>(todoItem);
        }

        public async Task<List<TodoItemDto>> GetAsync()
        {
            var todoItems = (await _todoItemRepository.GetAsync()).ToList();

            return _mapper.Map<List<TodoItemDto>>(todoItems);
        }

        public async Task InsertAsync(TodoItemDto todoItem)
        {
            await _todoItemRepository.InsertAsync(_mapper.Map<TodoItem>(todoItem));
        }

        public async Task UpdateAsync(TodoItemDto todoItem)
        {
            await _todoItemRepository.UpdateAsync(_mapper.Map<TodoItem>(todoItem));
        }

        public async Task DeleteAsync(int id)
        {
            await _todoItemRepository.DeleteAsync(id);
        }
    }
}
