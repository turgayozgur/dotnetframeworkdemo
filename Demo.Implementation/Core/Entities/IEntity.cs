﻿namespace Demo.Implementation.Core.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}