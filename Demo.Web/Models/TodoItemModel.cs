﻿namespace Demo.Web.Models
{
    public class TodoItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AdditionalData { get; set; }
        public bool IsComplete { get; set; }
    }
}