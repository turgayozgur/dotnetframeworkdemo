﻿namespace Demo.Implementation.Core.Entities
{
    public class TodoItem : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AdditionalData { get; set; }
        public bool IsComplete { get; set; }
    }
}
